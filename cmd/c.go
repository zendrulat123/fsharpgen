/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"os"
	"runtime"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/zendrulat123/fsharpgen/cmd/ut"
)

// cCmd represents the c command
var cCmd = &cobra.Command{
	Use:   "c",
	Short: "start cmd",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("c called")

		//used to specify slashes depending on os so no need to check os
		slash := string(os.PathSeparator)
		//get flag vars
		config, _ := cmd.Flags().GetString("config")
		name, _ := cmd.Flags().GetString("name")

		//create configs/files
		ut.CreateConfig(config)
		//write to file
		ut.WriteFile(config + slash + config + ".yaml")
		//add parent env field
		ut.ConfigAddEnv(config+slash+config+".yaml", "env:")
		//add field path and prj to config
		ut.ConfigAddFile(config+slash+config+".yaml", " path: "+slash+name+slash)
		ut.ConfigAddFile(config+slash+config+".yaml", " prj: "+slash+name+slash)
		//load config into memory
		viper.SetConfigName(config) // config file name without extension
		viper.SetConfigType("yaml")
		viper.AddConfigPath(".")
		viper.AddConfigPath("." + slash + config + slash) // config file path
		viper.AutomaticEnv()                              // read value ENV variable
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Println("fatal error config file: default \n", err)
			os.Exit(1)
		}
		// //structure to use flag vars
		type Data struct {
			Name string
		}
		d := Data{Name: name}
		//create base file
		m := ut.CreateBase(name)
		//use template string below to gen code
		mtm := template.Must(template.New("queue").Parse(mainTemplate))
		err = mtm.Execute(m, d)
		if err != nil {
			log.Print("execute: ", err)
		}

		if _, err := os.Stat(name); os.IsNotExist(err) {
			fmt.Println(slash+name, "does not exist")
			var and string
			if runtime.GOOS == "windows" {
				and = " ; "
			} else {
				and = " && "
			}
			fmt.Println(" dotnet new console -lang F# -o " + name + and + "cd " + name + and + " dotnet run ")
			//run bash command to install sdk
			err, out, errout := ut.Shellout(" dotnet new console -lang F# -o " + name + and + "cd " + name + and + " dotnet run ")
			if err != nil {
				log.Printf("error: %v\n", err)
			}
			fmt.Println(out)
			fmt.Println("--- errs ---")
			fmt.Println(errout)
		} else {
			fmt.Println("does exist")
		}

		defer m.Close()

	},
}

//main.go file
var mainTemplate = `

[<EntryPoint>]
let main argv = 
    printfn "Hello, World!"
    0 
  `

func init() {
	rootCmd.AddCommand(cCmd)
	cCmd.Flags().StringP("config", "c", "", "Set your config")
	cCmd.Flags().StringP("name", "n", "", "Set your file")

}
