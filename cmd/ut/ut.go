package ut

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
)

var slash = string(os.PathSeparator)

//creating bash commands
func Shellout(command string) (error, string, string) {
	var stdout bytes.Buffer
	var stderr bytes.Buffer
	cmd := exec.Command("bash", "-c", command)
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	return err, stdout.String(), stderr.String()
}

//creates the config folder/file
func CreateConfig(c string) {

	if err := os.MkdirAll(c, os.ModeSticky|os.ModePerm); err != nil {
		fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
	} else {
		fmt.Println("Whoops, could not create directory(ies) because", err)
	}

	mfile, err := os.Create(c + slash + c + ".yaml")
	if isError(err) {
		fmt.Println("error -", err, mfile)
	}
}

//create file command
func WriteFile(f string) {
	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(f, os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Save file changes.
	err = file.Sync()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("File Updated Successfully.")
}

//create field to config
func ConfigAddEnv(f string, E string) {
	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(f, os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Write some text line-by-line to file.
	_, err = file.WriteString(E + " \n")
	if err != nil {
		log.Fatal(err)
	}

	// Save file changes.
	err = file.Sync()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("File Updated Successfully.")
}

//add
func ConfigAddFile(f string, E string) {
	// Open file using READ & WRITE permission.
	var file, err = os.OpenFile(f, os.O_RDWR, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Write some text line-by-line to file.
	err = AppendStringToFile(f, E)
	if err != nil {
		log.Fatal(err)
	}
	err = AppendStringToFile(f, " \n")
	if err != nil {
		log.Fatal(err)
	}
	// Save file changes.
	err = file.Sync()
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("File Updated Successfully.")
}

//adding a string to the file
func AppendStringToFile(path, text string) error {
	f, err := os.OpenFile(path, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.WriteString(text)
	if err != nil {
		return err
	}
	return nil
}

//creates the main
func CreateBase(p string) *os.File {
	//split p to get the directory
	if err := os.MkdirAll(p, os.ModeSticky|os.ModePerm); err != nil {
		fmt.Println("Directory(ies) successfully created with sticky bits and full permissions")
	} else {
		fmt.Println("Whoops, could not create directory(ies) because", err)
	}

	//making fsharp file
	mfile, err := os.Create(p + slash + p + ".fs")
	if isError(err) {
		fmt.Println("error -", err, mfile)
	}

	return mfile

}

//just error check shortcut
func isError(err error) bool {
	if err != nil {
		fmt.Println(err.Error())
	}

	return (err != nil)
}
